# Short Story
## ZeroMQ Model
* black arrows: flow of data
* blue arrows: 'transport' description
* orange: Hardware
* grey: threads
* grey/green: asyncio coroutines in single thread
* boxes: logical blocks (e.g. classes)
* all sockets are PAIR

![](zepl-model.png)

# Long Story

[STEP-1](https://gitlab.com/zepl1/docs/-/blob/master/STEP-1.md)
